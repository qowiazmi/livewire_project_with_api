<?php

namespace App\Livewire\Forms;

use Livewire\Form;
use Livewire\Attributes\Rule;
use Livewire\Attributes\Validate;

class ProfilForm extends Form
{
    #[Rule('requaired', message: 'Isikan Nama', as:('form.nama'))]
    public $name = '';

    #[Rule('required', message: 'Isikan Website', as:('form.website'))]
    public $website = '';

    #[Rule('required', message: 'Isikan About', as:('form.about'))]
    public $about = '';

    #[Validate('required')]
    public $first_name = '';

    #[Rule('required')]
    public $last_name = '';

    #[Rule('required')]
    public $email = '';

    #[Rule('required')]
    public $country = '';
}
