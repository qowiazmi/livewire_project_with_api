<?php

namespace App\Livewire;

use GuzzleHttp\Client;
use LivewireUI\Modal\ModalComponent;
use PhpParser\Node\Expr\Cast\Object_;

class EditUser extends ModalComponent
{
    public $title = 'Edit User';
    public $users;
    public $sampleData;

    public function mount($id)
    {        
        $client = new Client(['verify' => false]);
        $url = "https://reqres.in/api/users/".$id;
        // GET Request
        $response = $client->request('GET', $url);
        $this->users = json_decode($response->getBody());
        $this->users = $this->users->data;
        //print_r($this->users);
    }

    public function update()
    {
        // $this->sampleData = $this->users->id;
        // dd($this->sampleData);
        $client = new Client(['verify' => false]);
        $url = 'https://reqres.in/api/users/'.$this->users->id;
        // PUT Request
        $response = $client->request('PUT', $url, [
            // 'id' => $this->users->id,
            // 'first_name' => $this->users->first_name,
            // 'last_name' => $this->users->last_name,
            // 'email' => $this->users->email,
        ]);
        // $response = $client->request('PUT', $url, [
        //     'id' => '',
        //     'first_name' => '',
        //     'last_name' => '',
        //     'email' => '',
        // ]);
        // return $response; 
        dd($response);      

    }

    // public function fun_name_post() { 
    //     $data = $this->security->xss_clean($_POST);
    //     $this->form_validation->set_data($data);
    //     $this->form_validation->set_rules('name', 'message', 'required');
    
    //     if ($this->form_validation->run() == FALSE) {
    //      // Form Validation Errors
    //      $this->set_response([
    //        'status' => FALSE,
    //        'error' => $this->form_validation->error_array(),
    //        'message' => validation_errors()
    //         ], REST_Controller::HTTP_NOT_FOUND);
    
        
    
    //       } else {
    
    //       // Success function
    
    //     }

    // }    

    public function render()
    {
        return view('livewire.edit-user');
    }
}