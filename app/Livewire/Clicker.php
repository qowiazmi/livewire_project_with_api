<?php

namespace App\Livewire;

use App\Livewire\Forms\ProfilForm;
use App\Models\User;
use GuzzleHttp\Client;
use Livewire\Attributes\Title;
use Livewire\Component;

#[Title('Clicker')]

class Clicker extends Component
{
    public ProfilForm $form;

    public $comboData = array();

    public function save()
    {
        $this->validate();


        // Post::create([
        //     'first_name' => $this->first_name,
        //     'last_name' => $this->last_name,
        // ]);
    }

    public function mount()
    {
        $client = new Client(['verify' => false]);
        $url = "https://reqres.in/api/users";
        $response = $client->request('GET', $url);
        $country = json_decode($response->getBody(),true);
        $i=0;
        foreach ($country['data'] as $item):
            $this->comboData[$i]['label']=$item['first_name'].' '.$item['last_name'];
            $this->comboData[$i]['value']=$item['id'];
            $i++;
        endforeach;

    }

    public function createNewUser ()
    {
        $this->validate([
            'name' => 'required|min:2|max:50',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:5'
        ]);

        User::create([
            'name' => $this->name,
            'email' => $this->email,
            'password' => $this->password
            // 'name' => "test_user",
            // 'email' => "test@test.com",
            // 'password' => "12321323442"
        ]);
    }

    public function render()
    {

        return view('livewire.clicker');
    }
}
