<?php

namespace App\Livewire;

use Illuminate\Mail\Mailables\Content;
use Livewire\Component;
use Livewire\Attributes\Title;

use function PHPSTORM_META\type;

#[Title('Form Toggle')]

class FormAddExample extends Component
{
    public $isTaSkripsi;
    public $isBerlaku;
    public $selectToggle;
    public $dataToggle;
    public $dataToggles;

    public function updatedIsTaSkripsi()
    {
        $this->dataToggle = $this->isTaSkripsi? 1: 0;
    }

    public function updatedIsBerlaku()
    {
        $this->dataToggle = $this->isBerlaku? 1: 0;
    }

    public function notificationAlert()
    {
        $this->dispatch('notify', content:'Data berhasil disimpan', type: 'success');
    }

    public function simpan()
    {
        // $this->dataToggle = gettype($this->isTaSkripsi);
        $this->dataToggle = $this->isTaSkripsi? 1: 0;
        $this->dataToggles = $this->isBerlaku? 1: 0;
    }

    public function render()
    {
        return view('livewire.form-add-example');
    }
}
