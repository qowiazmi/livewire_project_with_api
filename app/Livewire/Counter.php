<?php

namespace App\Livewire;

use Livewire\Attributes\Title;
use Livewire\Component;

#[Title('Dashboard Counter')]

class Counter extends Component
{     
    public function render()
    {
        $title = "Page Counter";
        return view('livewire.counter');
    }
}
