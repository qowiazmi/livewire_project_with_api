<?php

namespace App\Livewire;

use Livewire\Component;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Http;
use iluminate\support\Arr;
use Livewire\Attributes\Title;

#[Title('Project')]

class Projects extends Component
{
    // public $users = [
        
    //     [
    //     "id"=> 1,
    //     "email" => "george.bluth@reqres.in",
    //     "first_name" => "George",
    //     "last_name" => "Bluth",
    //     "title" => "Front-en Developer",
    //     "role" => "Staff",
    //     "avatar" => "https://reqres.in/img/faces/1-image.jpg"
    //     ],        
            
    //     [
    //     "id" => 2,
    //     "email" => "janet.weaver@reqres.in",
    //     "first_name" => "Janet",
    //     "last_name" => "Weaver",
    //     "title" => "Front-en Developer",
    //     "role" => "Dosen",
    //     "avatar" => "https://reqres.in/img/faces/2-image.jpg"
    //     ],
            
    //     [
    //     "id" => 3,
    //     "email" => "emma.wong@reqres.in",
    //     "first_name" => "Emma",
    //     "last_name" => "Wong",
    //     "title" => "Back-en Developer",
    //     "role" => "Staff",
    //     "avatar" => "https://reqres.in/img/faces/3-image.jpg"
    //     ],               
    // ];

    public $users;
    
    public function mount()
    {
        $client = new Client(['verify' => false]);

        $url = "https://reqres.in/api/users";
        // GET Request
        $response = $client->request('GET', $url);
        $users = json_decode($response->getBody());
        $this->users = $users->data;
    }

    public function render()
    {
        return view('livewire.projects');
    }
}
