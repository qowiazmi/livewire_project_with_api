<?php

namespace App\Livewire;

use GuzzleHttp\Client;
use LivewireUI\Modal\ModalComponent;
use Illuminate\Support\Facades\Http;
use Livewire\Attributes\Title;

#[Title('Create New Items')]

class Create extends ModalComponent
{
    public $title = 'Create User';
    public $data = array();

    public function render()
    {
        // $this->data = Http::get('https://reqres.in/api/register')->json();
        return view('livewire.create', ['data' => $this->data]);
    }

    public function create()
    {
        $client = new Client(['verify' => false]);
        $url = 'https://reqres.in/api/register'.$this->data;
        // $response = $client.Http::post($url,[
        //     'id' => '',
        //     'first_name' => '',
        //     'last_name' => '',
        //     'email' => '',
        // ]);
        $response = $client->request('POST', $url, [
            'username' => 'admin',
            'email' => 'admin@adm.com',
            'password' => 'admin123',
        ]);
        // return $response;
        dd($response->getBody());
       
    //    $response = Http::post('https://reqres.in/api/register', $this->data);

    //    $jsonData = $response->json();

    //    dd($jsonData);

        // $this->reset();
    }
}
