<?php

namespace App\Livewire;

use Livewire\Component;
use GuzzleHttp\Client;
use Livewire\Attributes\Title;

#[Title('Form Scracth')]

class FormScracth extends Component
{
    public $users;
    public $comboData = array();
    public $comboData2 = array();
    public $selectedData=2;
    public $selectedData2=0;
    public $selectedData3 = array(3,5);
    public $name = '';
    public $hasil;

    public function mount()
    {
        $client = new Client(['verify' => false]);

        $url = "https://reqres.in/api/users";
        // GET Request
        $response = $client->request('GET', $url);
        $users = json_decode($response->getBody(),true);
        //$this->users = $users->data;
        // print('<pre>');
        // print_r($users);
        // print('</pre>');
        $i=0;
        foreach ($users['data'] as $user):
            // print('<pre>');
            // print_r($user);
            // print('</pre>');
            $this->comboData[$i]['label']=$user['first_name'].' '.$user['last_name'];
            $this->comboData[$i]['value']=$user['id'];

            $this->comboData2[$i]['label']=$user['first_name'].' '.$user['last_name'];
            $this->comboData2[$i]['value']=$user['id'];
            $i++;
        endforeach;
        //$this->comboData = json_encode($this->comboData);
        // print('<pre>');
        // print_r($this->comboData);
        // print('</pre>');
    }

    public function updatedSelectedData()
    {
        // $property: The name of the current property that was updated
        $this->selectedData = $this->selectedData['value'];
        // print('<pre>');
        // print_r($this->selectedData);
        // print('</pre>');
    }

    public function simpan(){
        $this->hasil = $this->selectedData;
    }

    public function render()
    {
        return view('livewire.form-scracth');
    }
}
