<?php

use Illuminate\Support\Facades\Route;
use App\Livewire\Clicker;
use App\Livewire\Counter;
use App\Livewire\FormAddExample;
use App\Livewire\Projects;
use App\Livewire\FormScracth;
use App\Livewire\Home;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', Home::class, function () {
    return view('components.layouts.app');
});
Route::get('/clicker', Clicker::class, function () {
    return view('livewire.clicker');
});
Route::get('/projects', Projects::class, function () {
    return view('livewire.projects');
});
Route::get('/form_scracth', FormScracth::class, function () {
    return view('livewire.form-scracth');
});
Route::get('/form_toggle', FormAddExample::class, function () {
    return view('livewire.form-add-example');
});
