<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        @vite('resources/css/app.css')
        @vite('resources/fontawesome/css/all.min.css')
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/choices.js/public/assets/styles/choices.min.css" />
        <script src="https://cdn.jsdelivr.net/npm/choices.js/public/assets/scripts/choices.min.js"></script>
        <title>{{ $title ?? 'Page Title' }}</title>
    </head>
    <body>
        <div class="min-h-full">
            <livewire:header />
            <div class="py-5">
                <header>
                    <div class="mx-auto max-w-7xl px-4 sm:px-6 lg:px-8">
                        <h1 class="text-3xl font-bold leading-tight tracking-tight text-gray-900">Livewire</h1>
                    </div>
                </header>
                <main>
                    <div class="mx-auto max-w-7xl sm:px-6 lg:px-8 mt-5">
                    <!-- Your content -->
                    {{ $slot }}
                    </div>
                </main>
            </div>
            <livewire:footer />
        </div>
        <x-notifications />
        @livewire('wire-elements-modal')
    </body>
</html>
