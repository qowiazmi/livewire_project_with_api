<nav x-data="{ open: false }" class="border-b border-gray-200 bg-white">
    <div class="mx-auto max-w-7xl px-4 sm:px-6 lg:px-8">
        <div class="flex h-16 justify-between">
            <div class="flex">
                <div class="flex flex-shrink-0 items-center">
                    <a href="/">
                        <img class="block h-8 w-auto lg:hidden" src="{{url('/images/Logo-Purple - Horizontal.svg')}}" alt="Your Company">
                        <img class="hidden h-10 w-auto lg:block" src="{{url('/images/Logo-Purple - Horizontal.svg')}}" alt="Your Company">
                    </a>
                </div>
                <div class="hidden sm:-my-px sm:ml-6 sm:flex sm:space-x-8">
                </div>
            </div>
            <div class="hidden sm:ml-6 sm:flex sm:items-center">

                <!-- Profile dropdown -->
                <div class="relative ml-3">
                    <div class="flex">
                        <button type="button" @click="open = ! open" class="relative flex rounded-md bg-white text-sm focus:outline-none focus:ring-2 focus:ring-purple-600 focus:ring-offset-2" id="user-menu-button">
                            <span class="absolute -inset-1.5"></span>
                            <span class="sr-only">Open user menu</span>
                            <img class="h-8 w-8 rounded-md" src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAoHCBYWFRgWFRYYGBgYGBgYGBgYGBgYGBgYGhgZGRgYGhgcIS4lHB4rIRgYJjgmKy80NTU1GiQ7QDs0Py40NTEBDAwMDw8QGBIRGDQhGCExMTExNDQ0MTE0NDQ0NDQ0NDQxMTQ0PzQxMTQxMTE0NDQxMTE/NDQxPzE0ND8xNDE0NP/AABEIALEBHAMBIgACEQEDEQH/xAAcAAABBQEBAQAAAAAAAAAAAAADAAECBAUGBwj/xABAEAACAQIDBQUFBwIFAwUAAAABAgADEQQhMQUSQVFhBiIycYETUpGhsRRCYnLB0fCC4SM0ksLxB6KyJFNzdLT/xAAXAQEBAQEAAAAAAAAAAAAAAAAAAQID/8QAGxEBAQEBAAMBAAAAAAAAAAAAAAERAhIhUTH/2gAMAwEAAhEDEQA/APPXEgVl40YColppFGosnSEKyR0SGVhElhRB0lknNoaXex9XcrmmbBScsvErE5X9bf8AAnR7QoutweHqCRmDlrcMpzJOYnCViQQy+Ncxpn+E3y5a8p2+xdppi6YXw1V7u7e1yOAvazZi3X4yUilXGZ4HQsot0yccxYZ2vnlMuq5XOxsBnrysBkLDgfO3WbeMVUycsDcqDfMcLWPwImbimp875ZHI6EgW42Bvz4esVh7b2q/sty5s+Qz+5e5y6/qZzMu7Vq71Qj3e76/e+fnpKUCQaPIRQJ2jWjb0e8BrRR40Bo8UUBR40eA8aKMTAV5t9lFviAxzCIxGfE93j+YzDnTdk+6He4FyAL/hzOf9XLgIHak2F87XsBpbLkLZaD9oRKgXJw2gItu2vkDexy1GYta3ITMw+MW4GQuL3G8LgCxGhvnbU8Jqow+RzBFmtna5Gt73HnnzCRqk21NjfIkmwvnnk39WYytnBVSSy7vhs5FlF93u8zbVbcTrleW1oo6g5rujgFsRfPibHM/PjmK21a6IhdyBnkdTfoOXSB512g/zNa3vtrz4/O8pKZLEPvuz+8zN8TeJRNM1K0RMnnykGWEMDJyCiSgddUoi1xMvEJNoE2tAvhxJrVjD3Ylyl2phbSs6ZysiIcpFzI3jGAInOOuHIbfQ7rjjoG6EjTzkgkPSW0LGphdsLiP8OqdysoC77rk2QAD215b2tjyMobVwlSgGLqLANe/G+dwb8SciOfGDrYZKmTXBGjLky+R5dJS2rVxAo+xZt+mGDBhmQBfJr5jgeWUmLrnrxRRSKUUUUBRRRQHijRQHijRQHivGigKKKKAp2mwlVKK7wN7hjlfJgWPkbWF73HyPI4Sjvuq8zn0HGd7h1vYBRoLC2ZsbDowsLeXlAZKQyvcEZnK9rk3yJuBe1tBrD01YHIXU6ZNa17jIjibHL5nS3hsAQBe1jbNiVW2QFic7jS45yttHbVLDtZO+54DvZ6Hdtl/V9MoGn9oFNAz2QAGw58fDrbK989dLZnitubVNdg1+4L7q56m4La/zPhaCxmIeswaoLLmVW99feP8At04QVanlLIlqmiXhkQQaNbKWEMoiIN4aQKwB2i3Y7CR3oZduhEerTjuBwk6QvrMujPq0joJUNA3nSLSEz8ZTAOUsqWMWpRMbcymkKdxaP9kMrOMlUMOlPKaS4UW0g6qWEGKG7aQd+RlhkleokIo18Mj+JbH3lsD6jQzKr4AqTukMAbHgw/pOvpebbJKe0qRJJGjLvf1Lr+szWufesUiNDpnr8847UoFeKE9kYjSMKHFHKxWgNFHtFuwGikgh5RwhgQmrguz+IqAEJuKbd6oRTWx4jeILegMoopGYNjzGsVRix7zMx/ExP1gdls44PCLnXFRjqUAN+BA5Lr/M4Wl2jdrjDYYsLW33sVtqt7jdyJ0/CPSh2fwCbu+VUnesLi9gAL2v1OvSdTSHC0mtY5vFYSvWzxFa19VTT1JkaGDRB3V8yc2PmTNzEUs5VejGpjDqnOV3Qmbj4QawFTC2mtTGM2GOsSI3KdBhsLxtJNhQZPJfFgNTtnIGbeKwdkvMlqcsrNmKtSC3ZYdJC0o7sJEqS2lG0TpMNBo3ONXphhBs4EdK0oomiQZfw1uMlUQEXkFWEW2pi2QlDEUOcvYa9osSgIhWDVocpTqUjNurS5SpUAE0zjMenAIgLAHS9vjkfrLeJflKYaBkVMNusVPAkSQozW2lSBe/B1VvW1j8wfjMysxQgHQm1+XU9P2mVRFAyRo5aS4mGcm1gISrgCEYsSSAdMhAwXpwmGwxN7CW2p92X9mYc7vrAxK+GIMgtI3zm1tGjZhflK1ajkOpECp7GL7OZrNgSPCfQ/vK1RGUEkCwBJPT+fWBVFAR8PQu485PD02cAkWGvpL2Apd7e5fz6QN3ZdEIqrwGd+ZOZM16ZEycMTwl+iZK1KeusrHWW67ZSi4zgqbLfSS9mCvWOkIEvpJVguHpL8oSrheItHRbWl2kl5l0ZmJw43SOc5/EYa2U6zHUsphVFBPlNc1jqMl8LlK32abbU8oIURymtYx0yvGd7iBNS0G9aRVbFGCo1IsQRAobTTLSV4ZJUpuJaRpFW0g67SSnKQciQVqgmXiTcmaldrCZGJfOajNZ1UZwJaGxLSuFJzsbc+HxlZFqvvUhzRv+1v2P1g8RSFSnc+R6dZGvXWmiMbEVSy5G9qYNmItxv/4xYR9xip0OX95mtRLZuMzFN8qid0fjUeGx94C3mLcZtswZSOc57GYNX7hNmHgbgR7pghi8RS7rKKgHvAk+jCzfGFGxdArfMbozN+A84TZ21FUeID8xC/WZOPxzvYFdwa2zzPO5ljZ2yd8bz5A6DjbmYFrH7RR2ABB8tPU6CWMJh2YguRlwAtMbaOzjTNxmp48jyMLg9qOi23N+2hO9l0NtYHU1SLZZmc/jK4qt7NDdQQzsPvEaKvMX+Jz4CDb7RXyayLxAG6tuo1PqZdwVFVyXwrmSdWbmYD1wEUIOVz+0t4OlZR1z/aZ9Ylt5gN4gM1ugBP6S/sbGCqmfjXJh9GHQ/W8DSw65y4WAlNFhVkqwQ1CeEhuwlNYammcNIJShqYtLCpAVBaZaM75i00sHpnM+hSubkTRTISLENpP3ZylSpY2nSbQuVynMVk7+cvLPS/RNxC+x6SthjLZrS1INiUlGpUE1cQJkYmkBmJqM0NmvIsYMvIl5WR0e0u0qmUzVaWKT8IGrTqxM8rLHLSKlVN7zKxSy8SbwlfCBKT4iqLIi7yqcjUc5IgHIki55SpXJ7YxPswFHjYX/ACqdCep4dPOYSB6jKtyzMwVQSTmxsIsVWao7OxuzEkn+aCdF2Bwqtjae991XcdSFsPrf0kUbt1s32K4dF0WnuX5kan1JJmYj7yq3QT0T/qFsv2mHLAZ0zvenH+dJ5bgK1rofT9RJVnxspdxbiNIXD4lhkQHAys2o8jK+GqfEfy8uOgycccmH0MqCocO3iUoZaXB4c575+MGmHBGl5zW3KZSpbMCwsOGp/tA6h8LhlGbmVGqUV8CFjzOkzeztDfDE5gG2edrAfvNmvRCiBn1ajuQuQHurkB1POTrAKN0esJh13VLnU5DylPEvw4n+XgiGDxFsRTH3bm/W/dPyMzFqNh67buqMwsdGUHQ9CJdwab2JpgcDc+Qz/SVe0B/9Q55m8kW+q7TCVhUVXXwsL9RzB6iX1oi05fspi1RGR23RvBlJ0BORueANlnV7pirEFoS1Rp5SWGTKWAnCZtbkV96ESmDIvTzhEa0iprTtHfSIVLxIM5FVsQcpjVkBPWb9alcSi2HAllSzVTc7sFuTTZOEpuM9JdZsalahcXmVicNebaPwMi9EGaZscjXpkGCE2doYS2YmW1K01KzYZYZDBIhJllaJHiIW2tza3prCDI8K9RFALkKDpfMseSqMzMfFbRC3CZn3iMvRf3+Ey6ddmcuzEtoCc/WRXRYzbCUxZFu/vNYhfIaX+M5rGY56gbfdmLEEknlfK3mZGs8qkwKVJLtbzmjsLH/Z8TTrcFcb35DdX/7SfhKaDvj1+hkHXOB9AVt114FWGXIgjWeI9qdknD12A8JO8h6E5fDT0nff9PNre0w5oubvRyF9TTPg+BuvkBLnanYy4imRYb4zU9eI8j+0Dyiji9L5EceBmxhcWGyOvL9phYvCMjFWBBGWcErMNDIu/XdYarlMDtP41PMH6LIYDa9sny/F+8W3qgcI6kG2WWeo/sJUaHZg2RjzP9v0l7EveZWy66pSG8bE59cySMvWQq4xm8IsOZ/aS3FktHxGIAy48BK9rAu0ehQ4n1JgKhNaotNNL5+Q1mLfK43k5m39X+zmGJLVjxyXymLtk3rP6Tv6WFCIqKLWE4Lao/x3850cywjWF+ByPrLtDbNWkbI/d905j4H9JSwhF906H5HgY1VLGxgdXs/tioyrJb8aZj1Q5j0JnTYLGpVG9TdXHGxzHmNR6zyxUhqBKneUlWGhUkEeREzeWp1XqijOSFOcds7tTVSwqKKg5+F/iMj6idNg9s0auSvusfuv3T6HQ+hksrc6lXKaSbU7SSIbwtuEy0rNUGkG1PjC+yu14VkECruZSr7KXnSwlS0Cyq5SL1VQXdgo5sQB85m4rb1JQwRt9xoLHdvzudR5TlMVimdizsWPM/ToJvHO9Opxm2aNrAlz+FbD4taYmI2jfRAPMkn9BMsvBPUmmbWg2PYfe9B3R8pSxGNJyvlylR3vBmESepeC3iND6cJIiK0BhWvkcj/NDGMfdiMAT5EHkbwrUe4W6yDCbOEob9C/Qg+YyMCp2b2p9mxCVfu+FxzRsm+GTeaiex1VvpmDmDwI4TwsrYz03YXaKlT2fTeu9ihakF1d9y26FHHusmekCPaPYaVAWtnzGvnPPsTs5kbdYeR4EcxNja3bqs5IoqtNOBIDOeueQ+HrDdlKn2r2iV7uQocMSoIF7GwyN89R/wAyxZXOewklo9J0W09gtTBde+g1P3k/Nbh1mbuCYtsbkl/FVaMs0kA1kXqhZUao7sERSzHQDM/2EmWrbOU8bjCe4mdzYW1JOVhOl7NbHNMbzjvtr06TKSiuC3XdRUqtkBeypzF7G7dZ0ew+0lGqwRgabnJQ1irHkGHHztOkmOdtt9rePqbiE9J5tiH3qjt+L6Tv+0b2Qzzqkb3PMmVFhRDk3F+I+kroYZGgANS/hF+vD4w1FSMyfQaSZPL4ftELQDoZK8rgyW9A18DtutSyRzu+63eX4HT0tN/BdrkOVVCp95M1/wBJzHxM4sNHBkslWdWPVsBj6VUdx1Y20Hi9VOYj1FsZ5WjEEEZEaEZETawXaKulgzb68nzNuj6/EmS8/G539dm7wdpkr2hom1yyk6ggnd9RrNRXDAMMwRcEZgjzmLsalleZLW756j9YbelBTZh5EQ5M6uIzPAs0UUCNo9pK0YwIGKO4yjCAiZGOY9oA2m/2VcMHpN0ceuTfQfGYLS92erbmITk10PqLj5gQLdXs+5qMNEHe3uYPAdfpMTHYchiCLWyA4Acp6UTMja2yVcbw1gefMltYbB12RwysVYaMDa3n05iX8bgWTUXWZtWnbMafSB1+zu1lnArqd0jvbpurZWF1t4fIn1vKO169O+/QYMjG26L3Vjwsc7a2/wCL80DJISMwSD0yMlmrLY6PD7Iuf8Z90/8Atp3nH5yAQvlmfKbCvTw6XK+zXjZGBY299rFj8fKcZTx1VfDVqL5Ow+hgsRiHc7zs7Hm7Fj8TLiNLH7RNZ1a26i91F1tzJ5k/pHw2AepdURmK94kA2Ucy3CXux+wjiXIZW3FtdwbAG+YtbMkactZ6wmHSmgRFCoBkAPn1MDzittD2uGbePfpiz31PAN6/W846hp6ztO2mHRLsg3Wa4JGVwefOcZR0gGUyYaDWTtAmrxmMhFAKr3EkDBUfD53PzhLwJBpINB3iBgHVoRHlYNJB4FkZsBzB/SGpbQqU+6jkC97df4JTpv3h5H9JOoc4FGqLWPIyxIMtxaPSOQ+ECUQEVo4gKQMIZEiBCMgk7SPHzgK0RElImANoyuVIcaqQw81N/wBJJhIkQPRlcEBhoQGHkReWKI3gRMvYNTew9I8l3P8AQSn+2aFF7GAF9nBwVInE7a2Z7Jz7pnp1NQZhdpsEGQniIHmLrYxKZYxVOxPSVoEjNbs1sJ8XVCLkosXf3V5D8R4TKRCxAUEkkAAaknIAes917K7FXC4dUAG9bedvec6n9B0EA+DwKYemtOmoVVGnPmSeJ6xVjlGq1t5+kHiXygeeduXuR0nIUPDOi7YVbu056l4RAmsJBrJiA8i5yMlIPqBzP0zgFQWAEUV4xMBXiBjRXgTBivI3jFoBKL9/0knfOApnUyO/AsKYyGxI9ZAGInMH0gGBkgZASUByYwkbx4DmQqaeUleMYDAxGRp8uUlAiY1pJogIHXdkKm9QZfcqMPRgrfUtNRzYzm+xuI3alRL+NVYD8pIP/kPhOirnOBqYQw2MwiutjKWGeauGXegeZ7Q2Vu/acvCyAdBYsT9Jys9F7WVlRMXzL0aa/mNMM3wUmecwDUahUhl1Uhh5qbj6T3urtFWpo6aOiuLcmAI+s8BQz1PsxiS2DpE/cDJ/oYgfICB0FCRxjWBj4U3gtpNZG8oHl3aWpvO3nMunoJY2w93bzgVGQgJZMSAhIDwN+/5CFMBTzJMA29FeNFAV4rxjFAe8ZjGJkHMCaHu+Zgt6EbJR5QEC5E+nqIooBkjmPFAhHEeKAwiMeKANdTCRRQIGIRRQNLsx/mR+R/qs6ytrFFAt4fSbuzdI8UDzbt14q3/2h/8AmpzjYooDrPSOx3+SH/yP9RFFA6nA6CB2v4G8oooHkO0/GfOREUUBxJxRQGbSBw8UUA8iYooDRRRQGaBqaRRQC19BARRQP//Z" alt="">
                            <span class="text-purple-700 text-sm inline-flex font-medium italic items-center ml-3 -my-1">Jhon Doe<br>190302000</span>
                        </button>
                    </div>

                    <div x-show="open" class="absolute right-0 z-10 mt-2 w-48 origin-top-right rounded-md bg-white py-1 shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none">
                        <a href="/" class="block px-4 py-2 text-sm text-gray-700" x-state:on="Active" x-state:off="Not Active" role="menuitem" tabindex="-1" id="user-menu-item-0" >Dashboard</a>
                        <a href="#" class="block px-4 py-2 text-sm text-gray-700" role="menuitem" tabindex="-1" id="user-menu-item-2">Sign out</a>
                    </div>
                </div>
            </div>
            <div class="-mr-2 flex items-center sm:hidden">
                <!-- Mobile menu button -->
                <button type="button" @click="open = !open" class="relative inline-flex items-center justify-center rounded-md bg-white p-2 text-gray-400 hover:bg-gray-100 hover:text-gray-500 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2" aria-controls="mobile-menu" aria-expanded="false">
                    <span class="absolute -inset-0.5"></span>
                    <span class="sr-only">Open main menu</span>
                    <svg  class="block h-6 w-6" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" aria-hidden="true">
                        <!-- Menu open: "hidden", Menu closed: "block" -->
                        <path x-show="!open" stroke-linecap="round" stroke-linejoin="round" d="M3.75 6.75h16.5M3.75 12h16.5m-16.5 5.25h16.5" />
                        <!-- Menu open: "block", Menu closed: "hidden" -->
                        <path x-show="open" stroke-linecap="round" stroke-linejoin="round" d="M6 18L18 6M6 6l12 12" />
                    </svg>
                </button>
            </div>
        </div>
    </div>

    <!-- Mobile menu, show/hide based on menu state. -->
    <div x-show="open" class="sm:hidden" id="mobile-menu">
        <div class="space-y-1 pb-3 pt-2">
            <!-- Current: "border-indigo-500 bg-indigo-50 text-indigo-700", Default: "border-transparent text-gray-600 hover:border-gray-300 hover:bg-gray-50 hover:text-gray-800" -->
            <a href="/" class="border-indigo-500 bg-indigo-50 text-indigo-700 block border-l-4 py-2 pl-3 pr-4 text-base font-medium" aria-current="page">Dashboard</a>
            <a href="/clicker" class="border-transparent text-gray-600 hover:border-gray-300 hover:bg-gray-50 hover:text-gray-800 block border-l-4 py-2 pl-3 pr-4 text-base font-medium">Clicker</a>
            <a href="/projects" class="border-transparent text-gray-600 hover:border-gray-300 hover:bg-gray-50 hover:text-gray-800 block border-l-4 py-2 pl-3 pr-4 text-base font-medium">Projects</a>
            <a href="/form_scracth" class="border-transparent text-gray-600 hover:border-gray-300 hover:bg-gray-50 hover:text-gray-800 block border-l-4 py-2 pl-3 pr-4 text-base font-medium">Form Scracth</a>
            <a href="#" class="border-transparent text-gray-600 hover:border-gray-300 hover:bg-gray-50 hover:text-gray-800 block border-l-4 py-2 pl-3 pr-4 text-base font-medium">New</a>
        </div>
        <div class="border-t border-gray-200 pb-3 pt-4">
            <div class="flex items-center px-4">
                <div class="flex-shrink-0">
                    <img class="h-10 w-10 rounded-full" src="https://images.unsplash.com/photo-1472099645785-5658abf4ff4e?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80" alt="">
                </div>
                <div class="ml-3">
                    <div class="text-base font-medium text-gray-800">Tom Cook</div>
                    <div class="text-sm font-medium text-gray-500">tom@example.com</div>
                </div>
                <button type="button" class="relative ml-auto flex-shrink-0 rounded-full bg-white p-1 text-gray-400 hover:text-gray-500 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2">
                    <span class="absolute -inset-1.5"></span>
                    <span class="sr-only">View notifications</span>
                    <svg class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" aria-hidden="true">
                        <path stroke-linecap="round" stroke-linejoin="round" d="M14.857 17.082a23.848 23.848 0 005.454-1.31A8.967 8.967 0 0118 9.75v-.7V9A6 6 0 006 9v.75a8.967 8.967 0 01-2.312 6.022c1.733.64 3.56 1.085 5.455 1.31m5.714 0a24.255 24.255 0 01-5.714 0m5.714 0a3 3 0 11-5.714 0" />
                    </svg>
                </button>
            </div>
            <div class="mt-3 space-y-1">
                <a href="#" class="block px-4 py-2 text-base font-medium text-gray-500 hover:bg-gray-100 hover:text-gray-800">Your Profile</a>
                <a href="#" class="block px-4 py-2 text-base font-medium text-gray-500 hover:bg-gray-100 hover:text-gray-800">Settings</a>
                <a href="#" class="block px-4 py-2 text-base font-medium text-gray-500 hover:bg-gray-100 hover:text-gray-800">Sign out</a>
            </div>
        </div>
    </div>
</nav>
