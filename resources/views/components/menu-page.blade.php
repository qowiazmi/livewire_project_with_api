<!-- Menu Per Page -->
<div class="mb-6">
    <div class="sm:hidden">
      <label for="tabs" class="sr-only">Select a tab</label>
      <!-- Use an "onChange" listener to redirect the user to the selected tab URL. -->
      <select id="tabs" name="tabs" class="block w-full rounded-md border-gray-300 focus:border-indigo-500 focus:ring-indigo-500">
        <option selected>My Account</option>
        <option>Company</option>
        <option>Team Members</option>
        <option>Billing</option>
      </select>
    </div>
    <div class="hidden sm:block">
      <nav class="isolate flex divide-x divide-gray-100 rounded-lg shadow" aria-label="Tabs">
        <!-- Current: "text-gray-900", Default: "text-gray-500 hover:text-gray-700" -->
        <a href="/clicker" class="text-purple-800 hover:text-purple-700 rounded-l-lg group relative min-w-0 flex-1 overflow-hidden bg-blue-200 py-4 px-4 text-center text-sm font-medium hover:bg-blue-100 focus:z-10" aria-current="page">
            <span class="flex justify-center">
                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-6 h-6 mr-2">
                    <path stroke-linecap="round" stroke-linejoin="round" d="M15.042 21.672 13.684 16.6m0 0-2.51 2.225.569-9.47 5.227 7.917-3.286-.672ZM12 2.25V4.5m5.834.166-1.591 1.591M20.25 10.5H18M7.757 14.743l-1.59 1.59M6 10.5H3.75m4.007-4.243-1.59-1.59" />
                </svg>
                Clicker
            </span>
          <span aria-hidden="true" class="bg-purple-500 absolute inset-x-0 bottom-0 h-0.5"></span>
        </a>
        <a href="/projects" class="text-purple-700 hover:text-purple-500 group relative min-w-0 flex-1 overflow-hidden bg-blue-200 py-4 px-4 text-center text-sm font-medium hover:bg-blue-100 focus:z-10">
            <span class="flex justify-center">
                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-6 h-6 mr-2">
                    <path stroke-linecap="round" stroke-linejoin="round" d="M14.25 9.75 16.5 12l-2.25 2.25m-4.5 0L7.5 12l2.25-2.25M6 20.25h12A2.25 2.25 0 0 0 20.25 18V6A2.25 2.25 0 0 0 18 3.75H6A2.25 2.25 0 0 0 3.75 6v12A2.25 2.25 0 0 0 6 20.25Z" />
                </svg>
                Project
            </span>
          <span aria-hidden="true" class="bg-transparent absolute inset-x-0 bottom-0 h-0.5"></span>
        </a>
        <a href="form_scracth" class="text-purple-700 hover:text-purple-500 group relative min-w-0 flex-1 overflow-hidden bg-blue-200 py-4 px-4 text-center text-sm font-medium hover:bg-blue-100 focus:z-10">
            <span class="flex justify-center">
                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-6 h-6 mr-2">
                    <path stroke-linecap="round" stroke-linejoin="round" d="M19.5 14.25v-2.625a3.375 3.375 0 0 0-3.375-3.375h-1.5A1.125 1.125 0 0 1 13.5 7.125v-1.5a3.375 3.375 0 0 0-3.375-3.375H8.25m0 12.75h7.5m-7.5 3H12M10.5 2.25H5.625c-.621 0-1.125.504-1.125 1.125v17.25c0 .621.504 1.125 1.125 1.125h12.75c.621 0 1.125-.504 1.125-1.125V11.25a9 9 0 0 0-9-9Z" />
                </svg>
                From Scracth
            </span>
          <span aria-hidden="true" class="bg-transparent absolute inset-x-0 bottom-0 h-0.5"></span>
        </a>
        <a href="form_toggle" class="text-purple-700 hover:text-purple-500 rounded-r-lg group relative min-w-0 flex-1 overflow-hidden bg-blue-200 py-4 px-4 text-center text-sm font-medium hover:bg-blue-100 focus:z-10">
            <span class="flex justify-center">
                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-6 h-6 mr-2">
                    <path stroke-linecap="round" stroke-linejoin="round" d="M10.125 2.25h-4.5c-.621 0-1.125.504-1.125 1.125v17.25c0 .621.504 1.125 1.125 1.125h12.75c.621 0 1.125-.504 1.125-1.125v-9M10.125 2.25h.375a9 9 0 0 1 9 9v.375M10.125 2.25A3.375 3.375 0 0 1 13.5 5.625v1.5c0 .621.504 1.125 1.125 1.125h1.5a3.375 3.375 0 0 1 3.375 3.375M9 15l2.25 2.25L15 12" />
                  </svg>
                Form Toggle
            </span>
          <span aria-hidden="true" class="bg-transparent absolute inset-x-0 bottom-0 h-0.5"></span>
        </a>
      </nav>
    </div>
</div>
