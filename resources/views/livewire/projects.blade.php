<div>
    @section('page-title')
        Projects Table
    @endsection

    <x-menu-page />

    <x-breadcrumb />

    <div class="px-4 sm:px-6 lg:px-8 mt-6">
        <div class="sm:flex sm:items-center">
            <div class="sm:flex-auto">
            <h1 class="text-base font-semibold leading-6 text-gray-900">Users</h1>
            <p class="mt-2 text-sm text-gray-700">A list of all the users in your account including their name, title, email and role.</p>
            </div>
            <div class="mt-4 sm:ml-16 sm:mt-0 sm:flex-none">
            <button type="button" wire:click="$dispatch('openModal', {component: 'create'})" class="block rounded-md bg-indigo-600 px-3 py-2 text-center text-sm font-semibold text-white shadow-sm hover:bg-indigo-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600">Add user</button>
            </div>
        </div>
        <div class="mt-8 flow-root">
            <div class="-mx-4 -my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
                <div class="inline-block min-w-full py-2 align-middle sm:px-6 lg:px-8">
                    {{-- Form Edit --}}
                    {{-- <div class="flex gap-2 mt-2 mb-2">
                        <div>
                            <label for="name" class="block text-sm font-medium leading-6 text-gray-900">ID</label>
                            <div class="relative mt-2">
                            <input type="text" name="name" id="name" class="peer block w-full border-0 bg-gray-50 py-1.5 text-gray-900 focus:ring-0 sm:text-sm sm:leading-6" placeholder="1">
                            <div class="absolute inset-x-0 bottom-0 border-t border-gray-300 peer-focus:border-t-2 peer-focus:border-indigo-600" aria-hidden="true"></div>
                            </div>
                        </div>
                        <div>
                            <label for="name" class="block text-sm font-medium leading-6 text-gray-900">Nama</label>
                            <div class="relative mt-2">
                            <input type="text" name="name" id="name" class="peer block w-full border-0 bg-gray-50 py-1.5 text-gray-900 focus:ring-0 sm:text-sm sm:leading-6" placeholder="Jane Smith">
                            <div class="absolute inset-x-0 bottom-0 border-t border-gray-300 peer-focus:border-t-2 peer-focus:border-indigo-600" aria-hidden="true"></div>
                            </div>
                        </div>
                        <div>
                            <label for="name" class="block text-sm font-medium leading-6 text-gray-900">Title</label>
                            <div class="relative mt-2">
                            <input type="text" name="name" id="name" class="peer block w-full border-0 bg-gray-50 py-1.5 text-gray-900 focus:ring-0 sm:text-sm sm:leading-6" placeholder="Bluth">
                            <div class="absolute inset-x-0 bottom-0 border-t border-gray-300 peer-focus:border-t-2 peer-focus:border-indigo-600" aria-hidden="true"></div>
                            </div>
                        </div>
                        <div>
                            <label for="name" class="block text-sm font-medium leading-6 text-gray-900">Role</label>
                            <div class="relative mt-2">
                            <input type="text" name="name" id="name" class="peer block w-full border-0 bg-gray-50 py-1.5 text-gray-900 focus:ring-0 sm:text-sm sm:leading-6" placeholder="Bluth">
                            <div class="absolute inset-x-0 bottom-0 border-t border-gray-300 peer-focus:border-t-2 peer-focus:border-indigo-600" aria-hidden="true"></div>
                            </div>
                        </div>
                        <div class="my-8">
                            <button type="button" class="inline-flex w-full justify-center rounded-md bg-orange-500 px-3 py-2 text-sm font-semibold text-white shadow-sm hover:bg-orange-400 sm:ml-3 sm:w-auto" @click="">Update</button>
                            <button type="button" class="inline-flex w-full justify-center rounded-md bg-red-600 px-3 py-2 text-sm font-semibold text-white shadow-sm hover:bg-red-500 sm:ml-3 sm:w-auto" @click="">Delete</button>
                        </div>
                    </div> --}}

                    <table class="min-w-full divide-y divide-gray-300">
                        <thead>
                            <tr>
                            <th scope="col" class="px-3 py-3.5 text-left text-sm font-semibold text-gray-900">ID</th>
                            <th scope="col" class="py-3.5 pl-4 pr-3 text-left text-sm font-semibold text-gray-900 sm:pl-0">Nama</th>
                            <th scope="col" class="py-3.5 pl-4 pr-3 text-left text-sm font-semibold text-gray-900 sm:pl-0">Title</th>
                            <th scope="col" class="px-3 py-3.5 text-left text-sm font-semibold text-gray-900">Role</th>
                            <th scope="col" class="relative py-3.5 pl-3 pr-4 sm:pr-0">
                                <span class="sr-only">Edit</span>
                              </th>
                            </tr>
                        </thead>
                        <tbody class="divide-y divide-gray-200" wire:loading.class="opacity-50">
                            @foreach ($users as $user)
                                <tr>
                                    {{-- <td class="whitespace-nowrap px-3 py-4 text-sm text-gray-500">{{ $user['id'] }}</td> --}}
                                    <td class="whitespace-nowrap px-3 py-4 text-sm text-gray-500">{{ $user->id }}</td>
                                    <td class="whitespace-nowrap py-4 pl-4 pr-3 text-sm font-medium text-gray-900 sm:pl-0">
                                        <div class="flex" items-center>
                                            <div class="h-11 w-11 flex-shrink-0">
                                                {{-- <img class="h-11 w-11 rounded-full" src="{{ $user['avatar']}}"> --}}
                                                <img class="h-11 w-11 rounded-full" src="{{ $user->avatar}}">
                                            </div>
                                            <div class="ml-4">
                                                {{-- <div class="font-medium text-gray-900">{{ $user['first_name'].' '.$user['last_name'] }}</div> --}}
                                                <div class="font-medium text-gray-900">{{ $user->first_name.' '.$user->last_name }}</div>
                                                {{-- <div class="mt-1 text-gray-500">{{ $user['email'] }}</div> --}}
                                                <div class="mt-1 text-gray-500">{{ $user->email }}</div>
                                              </div>
                                        </div>
                                    </td>
                                    {{-- <td class="whitespace-nowrap py-4 pl-4 pr-3 text-sm font-medium text-gray-900 sm:pl-0">{{ $user['title'] }}</td> --}}
                                    <td class="whitespace-nowrap py-4 pl-4 pr-3 text-sm font-medium text-gray-900 sm:pl-0">{{ $user->last_name }}</td>
                                    {{-- <td class="whitespace-nowrap px-3 py-4 text-sm text-gray-500">{{ $user['role'] }}</td> --}}
                                    <td class="whitespace-nowrap px-3 py-4 text-sm text-gray-500">{{ $user->last_name }}</td>
                                    <td class="relative whitespace-nowrap py-4 pl-3 pr-4 text-right text-sm font-medium sm:pr-0">
                                        {{-- Edit Button --}}
                                        {{-- <a href="#" class="text-indigo-600 hover:text-indigo-900">Edit<span class="sr-only">, {{ $user['first_name'].' '.$user['last_name'] }}</span></a> --}}
                                        {{-- <a href="#" class="text-indigo-600 hover:text-indigo-900">Edit<span class="sr-only">, {{ $user->first_name.' '.$user->last_name}}</span></a> --}}
                                        {{-- <livewire:edit :user=$user wire:key="'edit-user'. now() . $user-id()"/> --}}
                                        <button wire:click="$dispatch('openModal', {component: 'edit-user', arguments: { id: {{ $user->id }} }})" class="text-indigo-600 hover:text-indigo-900">Edit</button>
                                      </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    {{-- <?php //print_r($users) ;?> --}}
                </div>
            </div>
        </div>
    </div>
</div>
