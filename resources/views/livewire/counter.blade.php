
<div class="mx-auto max-w-7xl sm:px-6 lg:px-8">
    <div class="border-b border-gray-200 pb-5 mt-5">
        <h3 class="text-base font-mono leading-6 text-gray-900">Hello Master..!</h3>
        <p class="mt-2 max-w-4xl text-sm text-gray-500">Workcation is a property rental website. Etiam ullamcorper massa viverra consequat, consectetur id nulla tempus. Fringilla egestas justo massa purus sagittis malesuada.</p>
      </div>

    <livewire:todo-list />
</div>
