<!-- Form Experience Toggle -->
<div>
    @section('page-title')
        Form Toggle
    @endsection

    <x-menu-page />

    <x-breadcrumb />

    <div class="app-wrapper mx-auto px-4 lg:px-6 mt-6">
        <div class="text-lg font-semibold leading-6 text-gray-900">Form - Toggle</div>

        {{-- <x-flash-alert /> --}}

        <form action="" wire:submit="simpan">
             <div class="mt-12 flex items-center gap-4 md:gap-x-6 md:gap-y-8">
                <div class="relative flex gap-x-3">
                    <!-- Toggle TA / Skripsi -->
                    <div
                    {{-- x-data="{ value: @entangle('isTaSkripsi').live }" --}}
                    x-data="{ value: @entangle('isTaSkripsi') }"
                    class="flex items-center justify-center"
                    x-id="['toggle-skripsi']"
                    >
                        <input type="hidden" name="isTaSkripsi" :value="value">

                        <!-- Button -->
                        <button
                            x-ref="toggle"
                            @click="value = ! value"
                            type="button"
                            role="switch"
                            :aria-checked="value"
                            :aria-labelledby="$id('toggle-skripsi')"
                            :class="value ? 'bg-violet-600' : 'bg-slate-300'"
                            class="relative inline-flex w-14 rounded-full py-1 transition"
                        >
                            <span
                                :class="value ? 'translate-x-7' : 'translate-x-1'"
                                class="bg-white h-6 w-6 rounded-full transition shadow-md"
                                aria-hidden="true"
                            ></span>
                        </button>
                    </div>

                    <!-- Label -->
                    <label
                        @click="$refs.toggle.click(); $refs.toggle.focus()"
                        :id="$id('toggle-skripsi')"
                        class="text-gray-900 font-medium my-2"
                    >
                    TA/Skripsi
                    </label>
                    @error('form.is_ta_skripsi')
                         <span class="text-sm italic mt-1.5 text-red-500">{{ $message }}</span>
                     @enderror
                </div>

                <div class="relative flex gap-x-3">
                    <!-- Toggle Berlaku / Tidak -->
                    <div
                    {{-- x-data="{ value: @entangle('isBerlaku').live }" --}}
                    x-data="{ value: @entangle('isBerlaku') }"
                    class="flex items-center justify-center"
                    x-id="['toggle-berlaku']"
                    >
                        <input type="hidden" name="isBerlaku" :value="value">

                        <!-- Button -->
                        <button
                            x-ref="toggle"
                            @click="value = ! value"
                            type="button"
                            role="switch"
                            :aria-checked="value"
                            :aria-labelledby="$id('toggle-berlaku')"
                            :class="value ? 'bg-violet-600' : 'bg-slate-300'"
                            class="relative inline-flex w-14 rounded-full py-1 transition"
                        >
                            <span
                                :class="value ? 'translate-x-7' : 'translate-x-1'"
                                class="bg-white h-6 w-6 rounded-full transition shadow-md"
                                aria-hidden="true"
                            ></span>
                        </button>
                    </div>

                    <!-- Label -->
                    <label
                        @click="$refs.toggle.click(); $refs.toggle.focus()"
                        :id="$id('toggle-berlaku')"
                        class="text-gray-900 font-medium my-2"
                    >
                    Berlaku/Tidak
                    </label>
                </div>
                @error('form.is_berlaku')
                    <span class="text-sm italic mt-1.5 text-red-500">{{ $message }}</span>
                @enderror
            </div>

            <div class="flex justify-end">
                 <div class="flex flex-col sm:flex-row w-full gap-4 mt-12">
                      <a wire:navigate href="/" class="inline-flex w-full justify-center items-center rounded-lg bg-gray-500 border-2 border-gray-300 px-3.5 py-2 text-sm font-semibold text-white shadow-sm hover:bg-gray-400">
                         <i class="fa-solid fa-fw fa-arrow-left-long mr-1.5"></i>
                         Kembali
                     </a>
                     <button type="submit" wire:click="notificationAlert()" class="inline-flex w-full justify-center items-center rounded-lg bg-violet-600 border-2 border-violet-400 px-3.5 py-2 text-sm font-semibold text-white shadow-sm hover:bg-violet-500">
                         <i wire:loading.remove class="fa-solid fa-fw fa-save mr-1.5"></i>
                         <i wire:loading class="fa-solid fa-fw fa-spinner mr-1.5 fa-spin"></i>
                         Simpan Data
                     </button>
                 </div>
            </div>
            <h1 class="mt-12">Nilai A : {{ $dataToggle }}</h1>
            <h1 class="mt-12">Nilai B : {{ $dataToggles }}</h1>
         </form>
    </div>
</div>
