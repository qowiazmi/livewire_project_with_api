<x-modals class="modal-lg">
    <x-slot name="title">
        {{ $title }}
    </x-slot>
  
    <x-slot name="content">
      <form action="">
        <div class="flex gap-2 mt-2">
          <div>
            <label for="name" class="block text-sm font-medium leading-6 text-gray-900">ID</label>
            <div class="relative mt-2">
              <input type="text" name="id" id="id" wire:model="users.id" class="peer block w-full border-0 bg-gray-50 py-1.5 text-gray-900 focus:ring-0 sm:text-sm sm:leading-6" placeholder="1">
              <div class="absolute inset-x-0 bottom-0 border-t border-gray-300 peer-focus:border-t-2 peer-focus:border-indigo-600" aria-hidden="true"></div>
            </div>
          </div>
          <div>
            <label for="name" class="block text-sm font-medium leading-6 text-gray-900">Full Name</label>
            <div class="relative mt-2">
              <input type="text" name="name" id="name" wire:model="users.first_name" class="peer block w-full border-0 bg-gray-50 py-1.5 text-gray-900 focus:ring-0 sm:text-sm sm:leading-6" placeholder="Jane Smith">
              <div class="absolute inset-x-0 bottom-0 border-t border-gray-300 peer-focus:border-t-2 peer-focus:border-indigo-600" aria-hidden="true"></div>
            </div>
          </div>          
          <div>
            <label for="name" class="block text-sm font-medium leading-6 text-gray-900">Last Name</label>
            <div class="relative mt-2">
              <input type="text" name="title" id="title" wire:model="users.last_name" class="peer block w-full border-0 bg-gray-50 py-1.5 text-gray-900 focus:ring-0 sm:text-sm sm:leading-6" placeholder="Bluth">
              <div class="absolute inset-x-0 bottom-0 border-t border-gray-300 peer-focus:border-t-2 peer-focus:border-indigo-600" aria-hidden="true"></div>
            </div>
          </div>
          <div>
            <label for="name" class="block text-sm font-medium leading-6 text-gray-900">email</label>
            <div class="relative mt-2">
              <input type="email" name="email" id="email" wire:model="users.email" class="peer block w-full border-0 bg-gray-50 py-1.5 text-gray-900 focus:ring-0 sm:text-sm sm:leading-6" placeholder="name@email.com">
              <div class="absolute inset-x-0 bottom-0 border-t border-gray-300 peer-focus:border-t-2 peer-focus:border-indigo-600" aria-hidden="true"></div>
            </div>
          </div>
        </div>
      </form>
    </x-slot>
  
    <x-slot name="buttons">
      <div class="mt-5 sm:mt-4 sm:flex sm:flex-row-reverse">
        <button type="button" class="inline-flex w-full justify-center rounded-md bg-red-600 px-3 py-2 text-sm font-semibold text-white shadow-sm hover:bg-red-500 sm:ml-3 sm:w-auto" wire:click="$dispatch('closeModal')">Cancel</button>
        <button type="submit" class="mt-3 inline-flex w-full justify-center rounded-md bg-indigo-600 px-3 py-2 text-sm font-semibold text-gray-50 shadow-sm ring-1 ring-inset ring-indigo-400 hover:bg-indigo-500 sm:mt-0 sm:w-auto" wire:click="create()">{{ isset($data['id']) ? 'Update' : 'Create' }}</button>
      </div>
    </x-slot>
  </x-modals>