<div>
    @section('page-title')
        Form Scracth
    @endsection

    <x-menu-page />

    <x-breadcrumb />

    <div class="mt-6">
        <h3>Single Select</h3>
        <div wire:ignore
            x-data="{
                multiple: false,
                options:@js($comboData),
                value: @entangle('selectedData'),
                {{-- value:2, --}}
                init() {
                    {{-- let choices = new Choices(this.$refs.select);
                    choices.setChoices(this.options); --}}
                    this.$nextTick(() => {
                        let choices = new Choices(this.$refs.select)

                        let refreshChoices = () => {
                            let selection = this.multiple ? this.value : [this.value]

                            choices.clearStore()
                            choices.setChoices(this.options.map(({ value, label }) => ({
                                value,
                                label,
                                selected: selection.includes(value),
                            })))
                        }

                        refreshChoices()

                        this.$refs.select.addEventListener('change', () => {
                            this.value = choices.getValue(true)
                            console.log(this.value);
                        })

                        this.$watch('value', () => refreshChoices())
                        this.$watch('options', () => refreshChoices())
                    })

                }
            }"
            class="max-w-sm w-full"
        >
            <select  x-ref="select" :multiple="multiple" wire:model.live="selectedData" ></select>
        </div>

        <label>Terpilih : {{ $selectedData }}</label><br /><br />

        <h3>Multi Select (masih PR untuk nangkap data ke variabel array)</h3>
        <div wire:ignore
            x-data="{
                multiple: true,
                options:@js($comboData),
                value: @entangle('selectedData3'),
                init() {
                    this.$nextTick(() => {
                        let choices = new Choices(this.$refs.select,{
                            removeItems: true,
                            removeItemButton: true
                        });

                        let refreshChoices = () => {
                            let selection = this.multiple ? this.value : [this.value]

                            choices.clearStore()
                            choices.setChoices(this.options.map(({ value, label }) => ({
                                value,
                                label,
                                selected: selection.includes(value),
                            })))
                        }

                        refreshChoices()

                        this.$refs.select.addEventListener('change', () => {
                            this.value = choices.getValue(true)
                            console.log(this.value);
                        })

                        this.$watch('value', () => refreshChoices())
                        this.$watch('options', () => refreshChoices())
                    })

                }
            }"
            class="max-w-sm w-full"
        >
            <select  x-ref="select" :multiple="multiple" wire:model.live="selectedData3" ></select>
        </div>
        <label>Terpilih : {{ implode($selectedData3) }}</label><br /><br />

        <br /><br />
        <div>
            <select wire:model.live="selectedData2">
                @foreach($comboData2 as $data):
                <option value="{{ $data['value'] }}">{{ $data['label'] }}</option>
                @endforeach
            </select>
            <label>Terpilih : {{ $selectedData2 }}</label><br /><br />

            <br /><br />
        </div>

        <div>
            <h3>Testing Form</h3>
            <form wire:submit.prevent="simpan">
                <div wire:ignore
                    x-data="{
                        multiple: false,
                        options:@js($comboData),
                        value: @entangle('selectedData'),
                        init() {
                            this.$nextTick(() => {
                                let choices = new Choices(this.$refs.select)

                                let refreshChoices = () => {
                                    let selection = this.multiple ? this.value : [this.value]

                                    choices.clearStore()
                                    choices.setChoices(this.options.map(({ value, label }) => ({
                                        value,
                                        label,
                                        selected: selection.includes(value),
                                    })))
                                }

                                refreshChoices()

                                this.$refs.select.addEventListener('change', () => {
                                    this.value = choices.getValue(true)
                                    console.log(this.value);
                                })

                                this.$watch('value', () => refreshChoices())
                                this.$watch('options', () => refreshChoices())
                            })

                        }
                    }"
                    class="max-w-sm w-full">
                    <select  x-ref="select" :multiple="multiple" wire:model.live="selectedData" ></select>
                    <button type="submit">Simpan</button>
                </div>

            </form>
            <label>hasil Simpan : {{ $hasil }}</label><br /><br />
        </div>
    </div>
</div>
